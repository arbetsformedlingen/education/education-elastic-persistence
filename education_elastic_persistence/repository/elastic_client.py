import logging
import sys

import certifi
from ssl import create_default_context
from elasticsearch.helpers import bulk
from elasticsearch import Elasticsearch
import settings
import time
from datetime import datetime

log = logging.basicConfig(level=logging.INFO)
log = logging.getLogger(__name__)

if settings.ES_USER and settings.ES_PWD:
    context = create_default_context(cafile=certifi.where())
    es = Elasticsearch([settings.ES_HOST], port=settings.ES_PORT,
                       use_ssl=True, scheme='https', ssl_context=context,
                       http_auth=(settings.ES_USER, settings.ES_PWD), timeout=20)
else:
    es = Elasticsearch([{'host': settings.ES_HOST, 'port': settings.ES_PORT}], timeout=20)
log.info(f"Elastic object is set using host: {settings.ES_HOST}:{settings.ES_PORT}, user: {settings.ES_USER}")


def _bulk_generator(documents, indexname, idkey):
    log.debug(f"(_bulk_generator) index: {indexname}, idkey: {idkey}")
    for document in documents:
        doc_id = create_doc_id(document, idkey)

        yield {
            '_index': indexname,
            '_id': doc_id,
            '_source': document
        }


def index_document(document, indexname, idkey='id'):
    doc_id = create_doc_id(document, idkey)
    return es.index(index=indexname, id=doc_id, body=document)


def create_doc_id(document, idkey):
    doc_id = '-'.join([document[key] for key in idkey]) \
        if isinstance(idkey, list) else document[idkey]
    return doc_id


def bulk_index(documents, indexname, idkey='id'):
    action_iterator = _bulk_generator(documents, indexname, idkey)

    result = bulk(es, action_iterator, request_timeout=30, raise_on_error=True, yield_ok=False)
    log.info(f"(bulk_index) result: {result[0]}")
    return result[0]


def index_exists(indexname):
    es_available = False
    fail_count = 0
    while not es_available:
        try:
            result = es.indices.exists(index=[indexname])
            es_available = True
            return result
        except Exception as e:
            if fail_count > 1:
                # Elastic has its own failure management, so > 1 is enough.
                log.error(f"Elastic not available after try: {fail_count}. Stop trying. {e}")
                raise e
            fail_count += 1
            log.warning(f"Connection fail: {fail_count} for index: {indexname} with {e}")
            time.sleep(1)


def alias_exists(aliasname):
    return es.indices.exists_alias(name=[aliasname])


def get_alias(aliasname):
    return es.indices.get_alias(name=[aliasname])


def put_alias(indexlist, aliasname):
    return es.indices.put_alias(index=indexlist, name=aliasname)


def setup_index(timestamp, es_alias, name, es_mapping):
    if timestamp is None:
        timestamp = datetime.now().strftime('%Y%m%d-%H.%M')
    es_index = format_index_name(es_alias, name, timestamp)

    create_index(es_index, es_mapping)
    return es_index


def format_alias_name(es_alias, name):
    es_alias_name = f"{es_alias}-{name}"
    return es_alias_name


def format_index_name(es_alias, name, timestamp):
    es_index_name = f"{es_alias}-{name}-{timestamp}"
    return es_index_name


def create_index(indexname, extra_mappings=None):
    if index_exists(indexname):
        log.info(f"Index {indexname} already exists ")
        return

    log.info(f"Creating index: {indexname}")

    basic_body = {
        "mappings": {
            "properties": {
                "timestamp": {
                    "type": "long"
                },
            }
        }
    }

    if extra_mappings:
        body = extra_mappings
        if 'mappings' in body:
            body.get('mappings', {}).get('properties', {})['timestamp'] = {'type': 'long'}
        else:
            body.update(basic_body)
    else:
        body = basic_body

    result = es.indices.create(index=indexname, body=body, wait_for_active_shards=1)
    if 'error' in result:
        log.error(f"Error: {result} on create index: {indexname} , mapping: {body}")
    else:
        log.info(f"New index created: {indexname} , mapping: {body}")


def add_indices_to_alias(indexlist, aliasname):
    actions = {
        "actions": [
            {"add": {"indices": indexlist, "alias": aliasname}}
        ]
    }
    response = es.indices.update_aliases(body=actions)
    log.info(f"Added: {actions}")
    return response


def update_alias(indexnames, old_indexlist, aliasname):
    actions = {
        "actions": []
    }
    for index in old_indexlist:
        actions["actions"].append({"remove": {"index": index, "alias": aliasname}})

    actions["actions"].append({"add": {"indices": indexnames, "alias": aliasname}})
    es.indices.update_aliases(body=actions)
    log.info(f"Updated: {actions}")
