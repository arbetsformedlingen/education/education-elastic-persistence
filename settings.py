import os

ES_HOST = os.getenv("ES_HOST", "127.0.0.1")
ES_PORT = os.getenv("ES_PORT", 9200)
ES_USER = os.getenv("ES_USER")
ES_PWD = os.getenv("ES_PWD")

# ES_EDUCATIONS_INDEX = os.getenv('ES_EDUCATIONS_INDEX', 'educations')
ES_EDUCATIONS_ALIAS = os.getenv('ES_EDUCATIONS_ALIAS', 'educations')

VERSION = '0.0.1'

education_mappings = {
    "mappings": {
        "properties": {
            "id": {
                "type": "keyword",
            }
        }
    }
}