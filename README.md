# Education elastic persistence

Project for using elastic source as storage

## Install and run localhost
The easiest way to set up an Elastic Search and Kibana localhost, which this code is dependent on, is to install in Docker:

- Make sure you have docker installed. On Windows you probably need to install and use 'Docker Windows'.
- Install an Elastic search for docker by running these 3 commands (the current dir in the terminal doesn't matter):
```
docker network create elastic
docker pull docker.elastic.co/elasticsearch/elasticsearch:7.14.1
docker run --name elastic-kll --net elastic -p 9200:9200 -p 9300:9300 -e "discovery.type=single-node" docker.elastic.co/elasticsearch/elasticsearch:7.14.1
```
- Install a Kibana for docker and connect it to Elastic by running these 2 commands:
```
docker pull docker.elastic.co/kibana/kibana:7.14.1
docker run --name kibana-kll --net elastic -p 5601:5601 -e "ELASTICSEARCH_HOSTS=http://elastic-kll:9200" docker.elastic.co/kibana/kibana:7.14.1
```
TODO: These commands might need to be updated when new versions are available.
- Additional good to know commands:
  - Stop elastic + kibana containers:
  ```
  docker stop elastic-kll
  docker stop kibana-kll
  ```
  - Remove elastic + kibana containers:
  ```
  docker network rm elastic
  docker rm elastic-kll
  docker rm kibana-kll
  
  - Start elastic + kibana containers (e.g. after computer restart):
  ```  
  docker start elastic-kll
  docker start kibana-kll
  ```
See elastic guide for more info and updates:
https://www.elastic.co/guide/en/kibana/current//docker.html