import itertools
import time
import logging
from datetime import datetime
import settings
from elasticsearch.exceptions import NotFoundError

from education_elastic_persistence.repository import elastic_client


class ElasticStore:

    def __init__(self):
        self.log = logging.getLogger(__name__)


    def _grouper(self, n, iterable):
        iterable = iter(iterable)
        return iter(lambda: list(itertools.islice(iterable, n)), [])

    # TODO Support only single item instead of bulk.
    def save_educations_to_elastic(self, educations):
        elastic_client.bulk_index(educations, self.es_index)

    def save_education_to_elastic(self, education):
        elastic_client.index_document(education, self.es_index)

    def calculate_time(self, start_time, len_ads):
        elapsed_time = time.time() - start_time
        m, s = divmod(elapsed_time, 60)
        self.log.info(f"Imported {len_ads} docs in: {int(m)} minutes {int(s)} seconds.")


    def start_new_save(self, name):
        self.log.info(f'Starting with version: {settings.VERSION}')
        timestamp_now = datetime.now().strftime('%Y%m%d-%H.%M')
        self.es_index = elastic_client.setup_index(timestamp_now, settings.ES_EDUCATIONS_ALIAS, name, settings.education_mappings)
        return self.es_index

    def finish_save(self, name):
        es_alias_name = elastic_client.format_alias_name(settings.ES_EDUCATIONS_ALIAS, name)
        self._change_alias([self.es_index], es_alias_name)

    def _change_alias(self, idxnames, aliasname):
        self.log.info(f"Setting alias: {aliasname} to point to: {idxnames}")
        try:
            if elastic_client.alias_exists(aliasname):
                oldindices = list(elastic_client.get_alias(aliasname).keys())
                elastic_client.update_alias(idxnames, oldindices, aliasname)
            else:
                elastic_client.add_indices_to_alias(idxnames, aliasname)
        except NotFoundError as e:
            self.log.error(f"Can't create alias: {aliasname}. Indices not found: {idxnames}.")
            raise e

